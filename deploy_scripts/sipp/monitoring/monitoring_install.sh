#! /bin/bash

SRC_DIR=/usr/local/src
OS_SRC_DIR=$SRC_DIR/opensips_shtrudel
GIT_REPO=https://shtrudel.unfuddle.com/git/shtrudel_voicenter-opensips/
GIT_BRANCH=2.4

apt-get update
apt-get install vim git curl build-essential pkg-config autoconf libpcap-dev libnet1 libnet1-dev libssl-dev

cd $SRC_DIR
git clone $GIT_REPO -b $GIT_BRANCH $OS_SRC_DIR
wget --no-check-certificate https://github.com/SIPp/sipp/releases/download/v3.5.2/sipp-3.5.2.tar.gz

tar -xzvf sipp-3.5.2.tar.gz
cd sipp-3.5.2

./configure --with-pcap --with-openssl
make
make install

cd $OS_SRC_DIR
cp deploy_scripts/sipp/monitoring/sipp-monitoring.cron /etc/cron.d/sipp-monitoring
cp -r deploy_scripts/sipp/monitoring/sipp-monitoring /usr/local/src/

cd /usr/local/src/sipp-monitoring/

ETH1IP=`/sbin/ifconfig lo | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`

sed -i -e "s/ETH1IP/$ETH1IP/g" /usr/local/src/sipp-monitoring/sipp-monitoring.sh
chmod +x /usr/local/src/sipp-monitoring/sipp-monitoring.sh
