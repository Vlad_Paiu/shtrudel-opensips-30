LOCAL_IP=209.163.136.216
LOCAL_PORT=5060
REGISTRAR=185.138.171.254
SCENARIO=uas-register.xml
CSV_FILE=register_users.csv

LINES_NO=`cat register_users.csv | wc -l`
LINES_NO=$((LINES_NO-1))

/usr/local/bin/sipp \
	-i $LOCAL_IP \
	-p $LOCAL_PORT \
	-sf $SCENARIO \
	-inf $CSV_FILE \
	-nd \
	-m $LINES_NO \
	$REGISTRAR
