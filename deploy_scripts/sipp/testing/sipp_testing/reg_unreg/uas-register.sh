RATE=$1
TOTAL_RUNS=$2

LOCAL_IP=209.163.136.216
LOCAL_PORT=5060
REGISTRAR=185.138.171.254
SCENARIO=uas-register.xml
CSV_FILE=uas.csv
TIMEOUT=10

/usr/local/bin/sipp \
	-i $LOCAL_IP \
	-sf $SCENARIO \
	-timeout $TIMEOUT \
	-inf $CSV_FILE \
	-m $2 \
	-r $1 \
	-nd \
	$REGISTRAR
