/*
 * Domain MI functions
 *
 * Copyright (C) 2006 Voice Sistem SRL
 *
 * This file is part of opensips, a free SIP server.
 *
 * opensips is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version
 *
 * opensips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * History:
 * --------
 *  2006-10-05  created (bogdan)
 */


#include "../../dprint.h"
#include "../../db/db.h"
#include "domain_mod.h"
#include "domain.h"
#include "hash.h"
#include "mi.h"

extern db_con_t* db_handle;
extern db_func_t domain_dbf;
extern str reload_procedure;
extern str delete_procedure;

/*
 * MI function to reload domain table
 */
mi_response_t *mi_domain_reload(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	if(db_mode==0)
		return init_mi_error( 500, MI_SSTR("command not activated"));

	if (reload_domain_table () == 1) {
		return init_mi_result_ok();
	} else {
		return init_mi_error( 500, MI_SSTR("Domain table reload failed"));
	}
}


/*
 * MI function to print domains from current hash table
 */
mi_response_t *mi_domain_dump(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	mi_response_t *resp;
	mi_item_t *resp_obj, *domains_arr;

	if(db_mode==0)
		return init_mi_error(500, MI_SSTR("command not activated"));

	resp = init_mi_result_object(&resp_obj);
	if (!resp)
		return 0;
	domains_arr = add_mi_array(resp_obj, MI_SSTR("Domains"));
	if (!domains_arr)
		goto error;

	if(hash_table_mi_print(*hash_table, domains_arr)< 0)
	{
		LM_ERR("Error while adding item\n");
		goto error;
	}

	return resp;

error:
	free_mi_response(resp);
	return 0;
}


#define dom_hash(_s)  core_case_hash( _s, 0, DOM_HASH_SIZE)

mi_response_t *mi_domain_get(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str domain;
	struct domain_list *np;
	unsigned int hash_val;
	mi_response_t *resp;
	mi_item_t *resp_obj;

	if(db_mode==0)
		return init_mi_error( 500, "command not activated", 21);

	if (get_mi_string_param(params, "domain",&domain.s, &domain.len) < 0)
		return init_mi_param_error();

	hash_val = dom_hash(&domain);
	np = (*hash_table)[hash_val];

	while (np != NULL) {
		if (np->domain.len == domain.len &&
		memcmp(np->domain.s,domain.s,domain.len) == 0)
			break;

		np = np->next;
	}

	if (np == NULL) {
		return init_mi_error( 404, MI_SSTR("Not Found"));
	}

	resp = init_mi_result_object(&resp_obj);
	if (!resp)
		return 0;

	if (np->attrs.s) 
		add_mi_string(resp_obj, np->domain.s,np->domain.len,
		np->attrs.s, np->attrs.len);
	else
		add_mi_string(resp_obj, np->domain.s,np->domain.len,
		" ", 1);
	
	return resp;
}

mi_response_t *mi_domain_set(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str domain,attrs,update_query_s;
	struct domain_list *np,*prev;
	unsigned int hash_val;
	char query_buf[1024];

	if(db_mode==0)
		return init_mi_error( 500, "command not activated", 21);

	if (get_mi_string_param(params, "domain",&domain.s, &domain.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "attrs",&attrs.s, &attrs.len) < 0)
		return init_mi_param_error();

	hash_val = dom_hash(&domain);
	np = (*hash_table)[hash_val];
	prev = NULL;

	while (np != NULL) {
		if (np->domain.len == domain.len &&
		memcmp(np->domain.s,domain.s,domain.len) == 0)
			break;

		prev = np;
		np = np->next;
	}

	if (np == NULL) {
		LM_INFO("XXX - installing new domain %.*s\n",domain.len,domain.s);

		if (hash_table_install(*hash_table, &domain, &attrs)==-1){
			return init_mi_error(500, MI_SSTR("Internal Error"));
		}

	} else {
		LM_INFO("XXX - replacing existing domain %.*s \n",domain.len,domain.s);

		if (prev == NULL) {
			(*hash_table)[hash_val] = np->next;
		} else {
			prev->next = np->next;
		}

		shm_free(np);

		if (hash_table_install(*hash_table, &domain, &attrs)==-1){
			return init_mi_error(500, MI_SSTR("Internal Error"));
		}

	}

	update_query_s.s = query_buf; 
	update_query_s.len = snprintf(query_buf,1024,"call %.*s('%.*s','%.*s')",
	reload_procedure.len,reload_procedure.s,
	domain.len,domain.s,
	attrs.len,attrs.s);

	if (update_query_s.len <= 0) {
		LM_ERR("DB query building failed\n");
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	if ( domain_dbf.raw_query(db_handle, &update_query_s, 0) < 0) {
		LM_ERR("DB query failed %.*s\n",update_query_s.len,update_query_s.s);
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	return init_mi_result_ok();
}

mi_response_t *mi_domain_del(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str domain,update_query_s;
	struct domain_list *np,*prev;
	unsigned int hash_val;
	char query_buf[1024];

	if(db_mode==0)
		return init_mi_error( 500, "command not activated", 21);

	if (get_mi_string_param(params, "domain",&domain.s, &domain.len) < 0)
		return init_mi_param_error();

	hash_val = dom_hash(&domain);
	np = (*hash_table)[hash_val];
	prev = NULL;

	while (np != NULL) {
		if (np->domain.len == domain.len &&
		memcmp(np->domain.s,domain.s,domain.len) == 0)
			break;

		prev = np;
		np = np->next;
	}

	if (np == NULL) {
		LM_INFO("XXX - domain %.*s not found \n",domain.len,domain.s);
		goto db_del;
	}

	LM_INFO("XXX - deleting existing domain %.*s \n",domain.len,domain.s);

	if (prev == NULL) {
		(*hash_table)[hash_val] = np->next;
	} else {
		prev->next = np->next;
	}

	shm_free(np);

db_del:
	update_query_s.s = query_buf; 
	update_query_s.len = snprintf(query_buf,1024,"call %.*s('%.*s')",
	delete_procedure.len,delete_procedure.s,
	domain.len,domain.s);

	if (update_query_s.len <= 0) {
		LM_ERR("DB query building failed\n");
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	if ( domain_dbf.raw_query(db_handle, &update_query_s, 0) < 0) {
		LM_ERR("DB query failed %.*s\n",update_query_s.len,update_query_s.s);
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	return init_mi_result_ok();
}
