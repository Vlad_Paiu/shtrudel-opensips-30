#! /bin/bash

SRC_DIR=/usr/local/src
OS_SRC_DIR=$SRC_DIR/opensips_shtrudel
GIT_REPO=https://shtrudel.unfuddle.com/git/shtrudel_voicenter-opensips/
GIT_BRANCH=2.4

apt-get install vim git curl build-essential pkg-config autoconf

cd $SRC_DIR
git clone $GIT_REPO -b $GIT_BRANCH $OS_SRC_DIR
git clone https://github.com/sippy/rtpproxy.git rtpproxy

git -C rtpproxy submodule update --init --recursive
cd rtpproxy
make
make install

cd $OS_SRC_DIR
cp deploy_scripts/rtpproxy/rtpproxy_rec.default /etc/default/rtpproxy

ETH0IP=`/sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`
ETH1IP=`/sbin/ifconfig lo | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`

sed -i -e "s/ETH0IP/$ETH0IP/g" /etc/default/rtpproxy
sed -i -e "s/ETH1IP/$ETH1IP/g" /etc/default/rtpproxy

mkdir -p /mnt/spool/dump

cp deploy_scripts/rtpproxy/rtpproxy.init /etc/init.d/rtpproxy
chmod +x /etc/init.d/rtpproxy
